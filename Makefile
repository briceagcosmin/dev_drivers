obj-m=NMEA_driver.o
KERNEL_VERSION= $(shell uname -r)
KERNEL_PATH= /lib/modules/$(KERNEL_VERSION)/build 

all:
	make -C $(KERNEL_PATH) M=$(PWD) modules
clean:
	make -C $(KERNEL_PATH) M=$(PWD) 
