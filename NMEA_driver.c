/*********************************************************************************************************************
  @Author             : Bebe-Cosmin Briceag ; briceagcosmin@gmail.com ; (+40 747057115) Romania
  @Module Description : The driver assures the transmission and reception of the data from the both side user and 
                        device. The driver is meant to work on the NV08C protocol using the NV08C GPS device .
                        Therefore, any device which is able to provide such an information will be compatible with this driver.
                        The driver was developed with the software quality as well as the performance in mind.
                        It was meant to run in any embedded linux environment.
                        The source code is delivered under the GPL license, hence, anyone is welcome to take,modify or
                        to contribute.
  @Revision           : $1.0 $2016
  *******************************************************************************************************************

  Naming Convention                 Below is presented the template used for naming convention
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number) 
  @Example                         : ps   = pointer to structure
                                     pu8  = pointer to unsigned byte
                                     pau8 = pointer to an array of unsigned byte 
                                     cps  = const pointer to structure (the pointer is constant)
                                     pcs  = pointer to const structure (the data is constant)

********************************************************************************************************************/


/*----------------------------------------------- I N C L U D E S -------------------------------------------------*/
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/spinlock.h>
#include <asm/uaccess.h>



/*----------------------------------------- E N D - O F - I N C L U D E S -----------------------------------------*/



/*------------------------------------------- M O D U L E - S C O P E ---------------------------------------------*/



/*------------------------------------ E N D - O F - M O D U L E - S C O P E --------------------------------------*/



/*-------------------------------------------- F I L E - S C O P E ------------------------------------------------*/
#define NV08C_DEVICE_NAME    "NV08C"
#define NV08C_AUTHOR         "Bebe-Cosmin Briceag"
#define NV08C_DEVICE_VERSION "1.0"
#define NV08C_DEVICE_LICENSE "GPL"


#define NV08C_u32_SELECT_DRIVER_MODE_COMMAND              ((u32)0x01)
#define NV08C_u32_SELECT_MULTIPLE_MESSAGES_COMMAND        ((u32)0x02)
#define NV08C_u32_DTM_COMMAND                             ((u32)0x03)
#define NV08C_u32_GBS_COMMAND                             ((u32)0x04)
#define NV08C_u32_GGA_COMMAND                             ((u32)0x05)
#define NV08C_u32_GLL_COMMAND                             ((u32)0x06)
#define NV08C_u32_GNS_COMMAND                             ((u32)0x07)
#define NV08C_u32_GSA_COMMAND                             ((u32)0x08)
#define NV08C_u32_GSV_COMMAND                             ((u32)0x09)
#define NV08C_u32_RMC_COMMAND                             ((u32)0x0A)
#define NV08C_u32_VTG_COMMAND                             ((u32)0x0B)
#define NV08C_u32_ZDA_COMMAND                             ((u32)0x0C)
#define NV08C_u32_ALVER_COMMAND                           ((u32)0x0D)
#define NV08C_u32_PAMOD_COMMAND                           ((u32)0x0E)
#define NV08C_u32_PASET_COMMAND                           ((u32)0x0F)
#define NV08C_u32_PKON1_COMMAND                           ((u32)0x10)
#define NV08C_u32_PNVTT_COMMAND                           ((u32)0x11)
#define NV08C_u32_POCWT_COMMAND                           ((u32)0x12)
#define NV08C_u32_PONAV_COMMAND                           ((u32)0x13)
#define NV08C_u32_PONME_COMMAND                           ((u32)0x14)
#define NV08C_u32_POPPS_COMMAND                           ((u32)0x15)
#define NV08C_u32_POPWR_COMMAND                           ((u32)0x16)
#define NV08C_u32_PORST_COMMAND                           ((u32)0x17)
#define NV08C_u32_PORZA_COMMAND                           ((u32)0x18)
#define NV08C_u32_PORZD_COMMAND                           ((u32)0x19)
#define NV08C_u32_PORZE_COMMAND                           ((u32)0x1A)
#define NV08C_u32_PORZX_COMMAND                           ((u32)0x1B)


#define NV08C_u64_COMMAND_MASK                        ((u64)0xFFFFFFFFFFFFFFFF)
#define NV08C_u64_NV08C_FORMAT_FLAG                   ((u64)0x01)
#define NV08C_u64_BINR_FORMAT_FLAG                    ((u64)0x02)


#define NV08C_u64_DTM_USED_FLAG                       ((u64)0x04)
#define NV08C_u64_GBS_USED_FLAG                       ((u64)0x08)                       
#define NV08C_u64_GGA_USED_FLAG                       ((u64)0x10)
#define NV08C_u64_GLL_USED_FLAG                       ((u64)0x20)
#define NV08C_u64_GNS_USED_FLAG                       ((u64)0x40)
#define NV08C_u64_GSA_USED_FLAG                       ((u64)0x80)
#define NV08C_u64_GSV_USED_FLAG                       ((u64)0x100)
#define NV08C_u64_RMC_USED_FLAG                       ((u64)0x200)
#define NV08C_u64_VTG_USED_FLAG                       ((u64)0x400)
#define NV08C_u64_ZDA_USED_FLAG                       ((u64)0x800)

#define NV08C_u8_MAX_FORMAT_USED                      ((u8)10)
#define NV08C_u8_MAX_COMMANDS_USED                    ((u8)25)
#define NV08C_u8_MAX_BUFFER_SIZE                      ((u16)128)

#define NV08C_au8_QUERY_SEQUENCE                      "$PORZB,"



MODULE_LICENSE(NV08C_DEVICE_LICENSE);
MODULE_AUTHOR (NV08C_AUTHOR);
MODULE_VERSION(NV08C_DEVICE_VERSION);


static ssize_t NV08C_t_Read          (struct file *ps_File, char __user *pas8_UserData,size_t t_Size, loff_t *pt_Offset);
static ssize_t NV08C_t_Write         (struct file *ps_File,const char __user *cpas8_UserData,size_t t_Size, loff_t *pt_Offset);
static s32     NV08C_s32_Open        (struct inode *ps_Inode, struct file *ps_File);
static s32     NV08C_s32_Release     (struct inode *ps_Inode, struct file *ps_File);
static long    NV08C_s64_DevControl  (struct file *ps_File, unsigned int u32_DevCommand, unsigned long u64_Arg);

static u32  NV08C_u32_InitDriver(void);
static u8   NV08C_u8_CreateSequence(const u32 cu32_Command,const u64 cu64_Arg,u8 *const cpu8_Size);
static u8   NMEA_u8_Crc8(u8* pu8_Sequence,const u16 cu16_Size);




typedef enum{ e_NMEA_MODE = 0,e_BINR_MODE = 1}DriverMode;


typedef struct
{
  struct cdev   s_Device;
  spinlock_t    t_DeviceLock;
  DriverMode    e_Mode;
  
} NV08C_DeviceProtocol;

typedef struct file_operations NV08C_FileOperations;
typedef struct class DeviceClass;
typedef dev_t  DeviceNumber;

static NV08C_FileOperations  NV08C_s_Fops;
static NV08C_DeviceProtocol *NV08C_ps_Protocol;
static DeviceClass          *NV08C_ps_DeviceClass;
static DeviceNumber          NV08C_t_DeviceNumber;
static const s8 *apas8_StaticFormat[NV08C_u8_MAX_COMMANDS_USED] =
  { "DTM","GBS","GGA","GLL","GNS","GSA","GSV","RMC"
    ,"VTG","ZDA","ALVER","PAMOD","PASET","PKON1","PNVTT"
    ,"POCWT","PONAV","PONME","POPPS","POPWR","PORST","PORZA"
    ,"PORZD","PORZE","PORZX"
  };
static u8 NV08C_au8_OutputDriverBuffer[NV08C_u8_MAX_BUFFER_SIZE];


/*--------------------------------------- E N D - O F - F I L E - S C O P E ---------------------------------------*/



/*--------------------------------- F U N C T I O N S - I M P L E M E N T A T I O N -------------------------------*/



/*==================================================================================================================
  @Function Name : NV08C_u32_Startup
  @Called by     : Kernel
  @Argument in   : None
  @Argument out  : None
  @Description   : Initialization of the driver
===================================================================================================================*/
s32 __init NV08C_s32_Startup(void)
{
  u32 u32_DriverState;

  u32_DriverState = NV08C_u32_InitDriver();

  if(u32_DriverState != (u32)1)
    {
      printk(KERN_ALERT "Driver initialization failed \n");
      goto exit_point;
    }

 exit_point:
  u32_DriverState = (u32)0;


  return u32_DriverState;
  
}

/*==================================================================================================================
  @Function Name : NV08C_v_Cleanup
  @Called by     : Kernel
  @Argument in   : None
  @Argument out  : None
  @Description   : Cleanup the device 
===================================================================================================================*/
void __exit NV08C_v_CleanupModule(void)
{

  unregister_chrdev_region(NV08C_t_DeviceNumber,1);

  device_destroy(NV08C_ps_DeviceClass,MKDEV(MAJOR(NV08C_t_DeviceNumber),0));
  
  cdev_del(&NV08C_ps_Protocol->s_Device);

  kfree(NV08C_ps_Protocol);

  class_destroy(NV08C_ps_DeviceClass);

  printk(KERN_INFO "Driver removed!");
  

}



/*==================================================================================================================
  @Function Name : NV08C_v_InitDriver
  @Called by     : Kernel
  @Argument in   : None
  @Argument out  : None
  @Description   : Function responsible to keep together all the initializations needed by the driver
===================================================================================================================*/
u32 NV08C_u32_InitDriver(void)
{
  u32 u32_InitState = (u32)1;
   
  NV08C_s_Fops.owner            = THIS_MODULE;
  NV08C_s_Fops.read             = NV08C_t_Read;
  NV08C_s_Fops.write            = NV08C_t_Write;
  NV08C_s_Fops.open             = NV08C_s32_Open;
  NV08C_s_Fops.release          = NV08C_s32_Release;
  NV08C_s_Fops.unlocked_ioctl   = NV08C_s64_DevControl;
  
  if(alloc_chrdev_region(&NV08C_t_DeviceNumber,0,1,NV08C_DEVICE_NAME) < (u32)0)
    {
      printk(KERN_ALERT "Kernel cannot assign the major number\n");
      u32_InitState = (u32)0;
      goto exit_point;
    }else
    {
      printk (KERN_ALERT "Kernel assigned the major number %X\n",NV08C_t_DeviceNumber);
    }
  
  
  NV08C_ps_DeviceClass = class_create(THIS_MODULE,NV08C_DEVICE_NAME);
  
  NV08C_ps_Protocol = kmalloc(sizeof(NV08C_DeviceProtocol),GFP_KERNEL);

  if(!NV08C_ps_Protocol)
    {
      printk(KERN_ALERT "Malloc failed\n");
      u32_InitState = (u32)0;
      goto exit_point;
    }

  NV08C_ps_Protocol->e_Mode = e_NMEA_MODE;
  /* by default NMEA mode , it will be overwritten by the user configuration using ioctl */

  spin_lock_init(&NV08C_ps_Protocol->t_DeviceLock);
  
  

  cdev_init(&NV08C_ps_Protocol->s_Device,&NV08C_s_Fops);
  
  NV08C_ps_Protocol->s_Device.owner = THIS_MODULE;

  if( cdev_add(&NV08C_ps_Protocol->s_Device,NV08C_t_DeviceNumber,1) < 0)
    {
      printk(KERN_ALERT "Cannot add device \n");
      u32_InitState = (u32)0;
    }
  if( !device_create(NV08C_ps_DeviceClass,NULL,MKDEV(MAJOR(NV08C_t_DeviceNumber),0),NULL,"NV08C"))
    {
      printk (KERN_ALERT "Device cannot be created \n");
      u32_InitState = (u32)0;
    }



  exit_point:
  {
    
    return u32_InitState;
  }

  
    
}


/*==================================================================================================================
  @Function Name : NV08C_u32_Read
  @Called by     : Kernel
  @Argument in   : ps_File, pt_Offset,t_Size
  @Argument out  : __user pas8_UserData
  @Description   : Routine responsible for reading data from the  /dev file system
===================================================================================================================*/
ssize_t NV08C_t_Read(struct file *ps_File, char __user *pas8_UserData,size_t t_Size, loff_t *pt_Offset)
{
  ssize_t t_State = (ssize_t)-1;

  t_State = 0;

  copy_to_user(pas8_UserData,"NOK",3);

  return t_State;
}


/*==================================================================================================================
  @Function Name : NV08C_u32_Write
  @Called by     : Kernel
  @Argument in   : ps_File,t_Size, pt_Offset
  @Argument out  : none
  @Description   : Routine responsible for writing data to  the  /dev file system
===================================================================================================================*/
ssize_t NV08C_t_Write(struct file *ps_File, const char __user *cpas8_UserData,size_t t_Size, loff_t *pt_Offset)
{
  ssize_t t_State = (ssize_t)-1;

  t_State = 0;

  

  
  return t_State;

}


/*==================================================================================================================
  @Function Name : NV08C_u32_Open
  @Called by     : Kernel
  @Argument in   : ps_Inode, ps_File
  @Argument out  : none
  @Description   : Called by kernel when the /dev is open
===================================================================================================================*/
s32 NV08C_s32_Open(struct inode *ps_Inode, struct file *ps_File)
{
  NV08C_DeviceProtocol *ps_Device;
  
  s32 s32_State = (s32)-1;


  ps_Device = container_of(ps_Inode->i_cdev, NV08C_DeviceProtocol,s_Device);

  if(ps_Device != NULL)
    {
      s32_State = (s32)0;
      ps_File->private_data = ps_Device;

      printk(KERN_ALERT "Device open \n");
    }


  return s32_State;
  
}


/*==================================================================================================================
  @Function Name : NV08C_s32_Release
  @Called by     : Kernel
  @Argument in   : ps_Inode, ps_File
  @Argument out  : none
  @Description   : Called by kernel when the /dev is closed
===================================================================================================================*/
s32 NV08C_s32_Release(struct inode *ps_Inode, struct file *ps_File)
{
  s32 s32_State = (s32)-1;

  s32_State = 0;


  return s32_State;

}


/*==================================================================================================================
  @Function Name : NV08C_s64_DevControl
  @Called by     : Kernel
  @Argument in   : ps_File, u32_DevCommand, u64_Arg
  @Argument out  : none
  @Description   : Used to configure the device
===================================================================================================================*/
long  NV08C_s64_DevControl( struct file *ps_File,unsigned int u32_DevCommand,unsigned long u64_Arg)
{
  
          NV08C_DeviceProtocol *ps_Dev  = (NV08C_DeviceProtocol*)ps_File->private_data;
          long                  l_state = (long)0;
          u8 u8_CommandSize             = (u8)0;
  
  switch(u32_DevCommand)
    {
    case NV08C_u32_SELECT_DRIVER_MODE_COMMAND :
      {

	if(u64_Arg == (u64)0)
	  {
	    ps_Dev->e_Mode = e_NMEA_MODE;
	    
	  }else if(u64_Arg == (u64)1)
	  {
	    ps_Dev->e_Mode = e_BINR_MODE;
	    
	  }else
	  {
	    goto invalid_state;
	  }
	break;
      }
      
    default :
      {
	printk(KERN_DEBUG "I've got the command %X with the argument %X\n",u32_DevCommand,u64_Arg);
	
	if(ps_Dev->e_Mode == e_NMEA_MODE)
	  {
	    spin_lock(&ps_Dev->t_DeviceLock);

	    memcpy(&NV08C_au8_OutputDriverBuffer[0],NV08C_au8_QUERY_SEQUENCE ,(size_t)7);
	    /* Put the query sequence */
	    
	    if (NV08C_u8_CreateSequence(u32_DevCommand,u64_Arg,&u8_CommandSize) == (u8)1)
	      {
		/* put the user command / commands*/
		
		
		NMEA_u8_Crc8(&NV08C_au8_OutputDriverBuffer[0],u8_CommandSize);
		     
	        /* Send data to the serial core */
		 
	      }
	    
	    spin_unlock(&ps_Dev->t_DeviceLock);
	    
	  }else
	  {
	    printk(KERN_INFO "BINR mode not supported\n");
	  }
	
	break;
      }
      
    }
  
 invalid_state:
  {
    printk(KERN_INFO "Unknown commad\n");
    l_state = (long)-1;
  }
 
  
  return l_state;
}



/*==================================================================================================================
  @Function Name : NV08C_u8_CreateSequence
  @Called by     : Driver
  @Argument in   : cu64_Arg, cu32_Command
  @Argument out  : cpu8_Size
  @Description   : Prepare the needed sequence of bytes based on the user desired message format
===================================================================================================================*/
u8 NV08C_u8_CreateSequence(const u32 cu32_Command,const u64 cu64_Arg,u8 *const cpu8_Size)
{
  u64  u64_ArgMask      = (u64)0x07;
  u32  u32_CommandMask  = (u32)0x01;
  u8   u8_CommandIndex  = (u8)0;
  u8   u8_ReservedBytes = (u8)7;
  u8   u8_State         = (u8)1;
  

  while(u32_CommandMask & cu32_Command)
    {
      /* Get a command */

      if(u8_CommandIndex <= (u8)9)
	{
	  /* NMEA standard message */
	  memcpy(&NV08C_au8_OutputDriverBuffer[u8_ReservedBytes],apas8_StaticFormat[u8_CommandIndex],(size_t)3);
	  u8_ReservedBytes += (u8)3;
	  
	}else
	{
	  /* Propietary message */
	  memcpy(&NV08C_au8_OutputDriverBuffer[u8_ReservedBytes],apas8_StaticFormat[u8_CommandIndex],(size_t)5);
	  u8_ReservedBytes += (u8)5;
	}
			       
      
      if(u64_ArgMask & cu64_Arg)
	{
	  /* Get the argument for the command */
	  NV08C_au8_OutputDriverBuffer[u8_ReservedBytes + (u8)1] = ',';

	  if(u8_CommandIndex == (u8)0)
	    {
	      NV08C_au8_OutputDriverBuffer[u8_ReservedBytes + (u8)2] = (u8)(u64_ArgMask & cu64_Arg);
	      u8_ReservedBytes += (u8)2;
	      
	    }else
	    {
	      /* Avoid shifting by zero */
	      NV08C_au8_OutputDriverBuffer[u8_ReservedBytes + (u8)2] = (u8)((u64_ArgMask & cu64_Arg) >> ((u8)3 * u8_CommandIndex));
	      u8_ReservedBytes += (u8)2;
	    }
	}else
	{
	  printk (KERN_INFO "The arguments must be greater than or equal to one \n");
	  goto invalid;
	}
      
      u32_CommandMask <<= (u32)1;
      u64_ArgMask     <<= (u64)3;
      u8_CommandIndex++;
    }

  *cpu8_Size = u8_CommandIndex;

 invalid:
  u8_State = 0;
  

  return u8_State;
  
}

  
/*==================================================================================================================
  @Function Name : NMEA_u8_Crc8
  @Called by     : Driver
  @Argument in   : pu8_Dequence, cu16_Size
  @Argument out  : none
  @Description   : Calculate the Cyclic Redundancy Check 
===================================================================================================================*/
static u8 NMEA_u8_Crc8(u8* pu8_Sequence,const u16 cu16_Size)
{
  u16 u16_SeqIndex = (u16)0;
  u8  u8_Sum       = (u8)0x00;

  while(u16_SeqIndex < cu16_Size)
    {

      u8_Sum ^= *(pu8_Sequence + u16_SeqIndex);
      u16_SeqIndex++;
    }

  *(pu8_Sequence + cu16_Size)            = '*';
  *(pu8_Sequence + (cu16_Size )+ ((u16)1)) = u8_Sum;
  

  return u8_Sum;

}



/*-------------------------- E N D - O F - F U N C T I O N S - I M P L E M E N T A T I O N ------------------------*/
module_init(NV08C_s32_Startup);
module_exit(NV08C_v_CleanupModule);
